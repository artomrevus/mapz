﻿namespace BuildItFaster.Models.Resources
{
    internal class Detail : IResource
    {
        public Detail()
        {
            Name = "detail";
        }

        public string Name { get; }
    }
}