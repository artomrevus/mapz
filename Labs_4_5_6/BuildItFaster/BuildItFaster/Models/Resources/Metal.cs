﻿namespace BuildItFaster.Models.Resources
{
    internal class Metal : IResource
    {
        public Metal()
        {
            Name = "metal";
        }

        public string Name { get; }
    }
}