﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.Resources
{
    public interface IResource
    {
        public string Name { get; }
    }
}
