﻿namespace BuildItFaster.Models.Resources
{
    internal class IronMine : IResource
    {
        public IronMine()
        {
            Name = "iron mine";
        }

        public string Name { get; }
    }
}