﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.ObserverDP
{
    public interface IParameterlessSubscriber
    {
        void InvokeMethod();
    }
}
