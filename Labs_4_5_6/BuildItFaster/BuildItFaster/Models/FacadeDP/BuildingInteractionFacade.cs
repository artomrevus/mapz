﻿using BuildItFaster.Models.Buildings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace BuildItFaster.Models.FacadeDP
{
    public class BuildingInteractionFacade
    {
        public BuildingInteractionFacade()
        {

        }

        public string GetBuildingInfo(IBuilding building)
        {
            return
                $"Input resources:\n\n" +
                $"Type: {building.InputResourceType?.Name ?? "-"}\n" +
                $"In building: {building.InputResourcesInBuilding?.ToString() ?? "-"}\n" +
                $"Required for work: {building.RequiredInputResourcesCnt?.ToString() ?? "-"}\n\n" +
                $"Output resources:\n\n" +
                $"Type: {building.OutputResourceType.Name ?? "No info"}\n" +
                $"Count: {building.OutputResourcesCnt}\n\n" +
                $"Producing time: {((double)building.ProducingTimeMiliseconds / (double)1000).ToString("0.00")} s";
        }

        public string GetBuildingNameAndLvl(IBuilding building)
        {
            return $"{building.Name} Lvl {building.CurrentLevel}";
        }

        public void SupplyInputResources(IBuilding buildingWhereSupply, uint suppliedResourcesCnt, LevelData levelData)
        {
            if(buildingWhereSupply is MineBuilding)
            {
                throw new InvalidOperationException("Cant supply resources to MineBuilding!");
            }

            if (buildingWhereSupply is ForgeBuilding && levelData.IronMineCurrCnt < suppliedResourcesCnt ||
                buildingWhereSupply is MetalurgicalPlantBuilding && levelData.MetalCurrCnt < suppliedResourcesCnt)
            {
                throw new InvalidOperationException("Not enough resources to supply!");
            }

            if (buildingWhereSupply is ForgeBuilding)
            {
                levelData.IronMineCurrCnt -= (int)suppliedResourcesCnt;
            }
            else if (buildingWhereSupply is MetalurgicalPlantBuilding)
            {
                levelData.MetalCurrCnt -= (int)suppliedResourcesCnt;
            }

            buildingWhereSupply.InputResourcesInBuilding += suppliedResourcesCnt;
        }


        private object _locker = new object();
        public void ProduceResources(IBuilding building, LevelData levelData)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    if(!levelData.IsLevelContinuing)
                    {
                        break;
                    }

                    if (building is MineBuilding || building.InputResourcesInBuilding >= building.RequiredInputResourcesCnt)
                    {
                        building.Produce();

                        lock (_locker)
                        {
                            if (building is MineBuilding)
                            {
                                levelData.IronMineCurrCnt += (int)building.OutputResourcesCnt;
                            }
                            else if (building is ForgeBuilding)
                            {
                                levelData.MetalCurrCnt += (int)building.OutputResourcesCnt;
                            }
                            else if (building is MetalurgicalPlantBuilding)
                            {
                                levelData.DetailsCurrCnt += (int)building.OutputResourcesCnt;
                            }
                        }
                    }
                }
            }
            );
        }

    }
}
