﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.SingletonDP
{
    public class CurrentUser
    {
        private CurrentUser()
        {
            // private ctor
        }

        private static CurrentUser? _instance;
        private static readonly object _lock = new();

        public static CurrentUser Instance 
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                    {
                        _instance = new CurrentUser();
                    }

                    return _instance;
                }
            }
        }

        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
        public uint TotalPoints { get; set; }
        public bool IsHasPremium { get; set; }
    }
}
