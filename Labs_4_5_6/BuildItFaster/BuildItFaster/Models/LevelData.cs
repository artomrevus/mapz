﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;
using BuildItFaster.Models.ObserverDP;

namespace BuildItFaster.Models
{
    public class LevelData : INotifyPropertyChanged
    {
        private int _ironMineCurrCnt;
        public int IronMineCurrCnt
        {
            get { return _ironMineCurrCnt; }
            set
            {
                if (_ironMineCurrCnt != value)
                {
                    _ironMineCurrCnt = value;
                    OnPropertyChanged(nameof(IronMineCurrCnt));
                    CheckIfAllResourcesReachedRequiredCnt();
                }
            }
        }

        private int _metalCurrCnt;
        public int MetalCurrCnt
        {
            get { return _metalCurrCnt; }
            set
            {
                if (_metalCurrCnt != value)
                {
                    _metalCurrCnt = value;
                    OnPropertyChanged(nameof(MetalCurrCnt));
                    CheckIfAllResourcesReachedRequiredCnt();
                }
            }
        }

        private int _detailsCurrCnt;
        public int DetailsCurrCnt
        {
            get { return _detailsCurrCnt; }
            set
            {
                if (_detailsCurrCnt != value)
                {
                    _detailsCurrCnt = value;
                    OnPropertyChanged(nameof(DetailsCurrCnt));
                    CheckIfAllResourcesReachedRequiredCnt();
                }
            }
        }


        public int IronMineRequiredCnt { get; set; }
        public int MetalRequiredCnt { get; set; }
        public int DetailsRequiredCnt { get; set; }

        public IList<IBuilding?>? Buildings { get; set; }
        public string? BackgroundImg { get; set; }
        public bool IsLevelContinuing {  get; set; }



        public event PropertyChangedEventHandler? PropertyChanged;
        

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }





        // -------------------------------------------------------------------
        // Observer
        // -------------------------------------------------------------------

        public List<IParameterlessSubscriber>? Subscribers = new();

        public void Subscribe(IParameterlessSubscriber subscriber)
        {
            Subscribers?.Add(subscriber);
        }

        public void Unsubscribe(IParameterlessSubscriber subscriber)
        {
            Subscribers?.Remove(subscriber);
        }

        private void NotifySubscribersAllResourcesReachedRequiredCnt()
        {
            if (Subscribers is null)
            {
                return;
            }

            foreach (var subscriber in Subscribers)
            {
                subscriber.InvokeMethod();
            }
        }

        private void CheckIfAllResourcesReachedRequiredCnt()
        {
            if (IsLevelContinuing && IsAllResourcesReachedRequiredCnt())
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    NotifySubscribersAllResourcesReachedRequiredCnt();
                    IsLevelContinuing = false;
                });
            }
        }

        public bool IsAllResourcesReachedRequiredCnt()
        {
            return IronMineCurrCnt >= IronMineRequiredCnt && MetalCurrCnt >= MetalRequiredCnt && DetailsCurrCnt >= DetailsRequiredCnt;
        }
        // -------------------------------------------------------------------
   

    }
}
