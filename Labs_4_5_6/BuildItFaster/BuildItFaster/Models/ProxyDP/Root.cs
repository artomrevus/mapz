﻿namespace BuildItFaster.Models.ProxyDP
{
    public record Root(
        int fileSizeBytes,
        string url
    );
}
