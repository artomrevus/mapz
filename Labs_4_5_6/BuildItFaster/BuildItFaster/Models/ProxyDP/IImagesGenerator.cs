﻿namespace BuildItFaster.Models.ProxyDP
{
    public interface IImagesGenerator
    {
        string? GenerateRandomImage();
    }
}