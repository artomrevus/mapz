﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.ProxyDP
{
    public class ProxyImagesGenerator : IImagesGenerator
    {
        private IImagesGenerator _imagesGenerator;
        private const string alternateImage = "\\\\Mac\\Home\\Documents\\BuildItFaster\\BuildItFaster\\Resources\\Images\\Background\\ProxyBackground.jpg";

        public ProxyImagesGenerator(IImagesGenerator imagesGenerator)
        {
            _imagesGenerator = imagesGenerator;
        }

        public string? GenerateRandomImage()
        {
            if(!IsInternetAvailable())
            {
                Debug.WriteLine("Proxy: No internet connection to generate image.");
                return alternateImage;
            }

            string? generatedImage = _imagesGenerator.GenerateRandomImage();
            if (generatedImage is null) 
            {
                Debug.WriteLine("Proxy: Error while getting image.");
                return alternateImage;
            }
            else
            {
                Debug.WriteLine("Proxy: Image generated successfully.");
                return generatedImage;
            }
        }

        private bool IsInternetAvailable()
        {
            try
            {
                using (var ping = new Ping())
                {
                    var reply = ping.Send("8.8.8.8", 1000); // Google Public DNS
                    return reply.Status == IPStatus.Success;
                }
            }
            catch (PingException)
            {
                return false;
            }
        }
    }
}
