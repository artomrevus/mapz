﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace BuildItFaster.Models.ProxyDP
{
    public class DogImagesGenerator : IImagesGenerator
    {
        public string? GenerateRandomImage()
        {
            string apiUrl = "https://random.dog/woof.json";

            var response = GetRequestApi(apiUrl);
            if (response is not null)
            {
                Root? root = JsonSerializer.Deserialize<Root>(response);
                if (root is not null)
                {
                    return root.url;
                }
            }

            return null;
        }

        private string? GetRequestApi(string apiUrl)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    string response = client.DownloadString(apiUrl);
                    return response;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("Error while interacting with the API: " + ex.Message);
                }
            }

            return null;
        }
    }
}
