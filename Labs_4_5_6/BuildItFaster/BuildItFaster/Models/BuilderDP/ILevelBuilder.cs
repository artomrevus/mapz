﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;

namespace BuildItFaster.Models.BuilderDP
{
    public interface ILevelBuilder
    {
        ILevelBuilder SetStartResources(int ironMineCount, int metalCount, int detailsCount);
        ILevelBuilder SetRequiredResources(int ironMineCount, int metalCount, int detailsCount);
        ILevelBuilder SetStartBuildings(IList<IBuilding> buildings);
        ILevelBuilder SetBackground(string backgroundImg);
        LevelData GetBuildingResult();
    }
}
