﻿using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.BuilderDP.TemplateMethodDP
{
    public class HardLevelBuildDirector : AbstractLevelBuildDirector
    {
        public HardLevelBuildDirector(ILevelBuilder levelBuilder, IBuildingFactory startBuildingFactory) : base(levelBuilder, startBuildingFactory)
        {
        }

        protected override void SetStartResources()
        {
            _levelBuilder.SetStartResources(100, 0, 0);
        }

        protected override void SetRequiredResources()
        {
            _levelBuilder.SetRequiredResources(2000, 300, 150);
        }

        protected override void SetStartBuildings()
        {
            _levelBuilder.SetStartBuildings(
                new List<IBuilding>()
                {
                    _startBuildingFactory.CreateLvl1Building(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                });
        }
    }
}
