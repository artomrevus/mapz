﻿using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.BuilderDP.TemplateMethodDP
{
    public class MediumLevelBuildDirector : AbstractLevelBuildDirector
    {
        public MediumLevelBuildDirector(ILevelBuilder levelBuilder, IBuildingFactory startBuildingFactory) : base(levelBuilder, startBuildingFactory)
        {
        }

        protected override void SetStartResources()
        {
            _levelBuilder.SetStartResources(100, 20, 0);
        }

        protected override void SetRequiredResources()
        {
            _levelBuilder.SetRequiredResources(1000, 300, 150);
        }

        protected override void SetStartBuildings()
        {
            _levelBuilder.SetStartBuildings(
                new List<IBuilding>()
                {
                    _startBuildingFactory.CreateLvl1Building(),
                    _startBuildingFactory.CreateLvl1Building(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                });
        }
    }
}
