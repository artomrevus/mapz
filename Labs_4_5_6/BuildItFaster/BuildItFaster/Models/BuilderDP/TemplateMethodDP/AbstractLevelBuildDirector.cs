﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;
using BuildItFaster.Models.ProxyDP;
using Color = System.Drawing.Color;

namespace BuildItFaster.Models.BuilderDP.TemplateMethodDP
{
    public abstract class AbstractLevelBuildDirector
    {
        protected ILevelBuilder _levelBuilder;
        protected IBuildingFactory _startBuildingFactory;

        public AbstractLevelBuildDirector(ILevelBuilder levelBuilder, IBuildingFactory startBuildingFactory)
        {
            _levelBuilder = levelBuilder;
            _startBuildingFactory = startBuildingFactory;
        }

        public ILevelBuilder LevelBuilder
        {
            set { _levelBuilder = value; }
        }

        public void BuildLevel()
        {
            SetStartResources();
            SetRequiredResources();
            SetStartBuildings();

            _levelBuilder.SetBackground(GenerateRandomBackground());
        }

        protected abstract void SetStartResources();
        protected abstract void SetRequiredResources();
        protected abstract void SetStartBuildings();

        private string GenerateRandomBackground()
        {
            IImagesGenerator imagesGenerator = new ProxyImagesGenerator(new DogImagesGenerator());

            return imagesGenerator.GenerateRandomImage()!;
        }
    }
}
