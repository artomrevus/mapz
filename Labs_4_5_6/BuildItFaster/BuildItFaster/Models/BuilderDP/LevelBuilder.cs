﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;

namespace BuildItFaster.Models.BuilderDP
{
    internal class LevelBuilder : ILevelBuilder
    {
        private LevelData _levelData = new();

        public LevelData GetBuildingResult()
        {
            return _levelData;
        }

        public ILevelBuilder SetBackground(string backgroundImg)
        {
            _levelData.BackgroundImg = backgroundImg;

            return this;
        }

        public ILevelBuilder SetStartResources(int ironMineCount, int metalCount, int detailsCount)
        {
            _levelData.IronMineCurrCnt = ironMineCount;
            _levelData.MetalCurrCnt = metalCount;
            _levelData.DetailsCurrCnt = detailsCount;

            return this;
        }

        public ILevelBuilder SetRequiredResources(int ironMineCount, int metalCount, int detailsCount)
        {
            _levelData.IronMineRequiredCnt = ironMineCount;
            _levelData.MetalRequiredCnt = metalCount;
            _levelData.DetailsRequiredCnt = detailsCount;

            return this;
        }

        public ILevelBuilder SetStartBuildings(IList<IBuilding> buildings)
        {
            _levelData.Buildings = buildings;

            return this;
        }
    }
}
