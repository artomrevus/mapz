﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BuildItFaster.Models.Buildings;
using BuildItFaster.Models.Resources;

namespace BuildItFaster.Models.AbstractFactoryDP
{
    internal class MetalurgicalPlantBuildingFactory : IBuildingFactory
    {
        public IBuilding CreateLvl1Building()
        {
            return new MetalurgicalPlantBuilding(0, 10, 5, 5_000);
        }

        public IResource? CreateInputResource()
        {
            return new Metal();
        }

        public IResource CreateOutputResource()
        {
            return new Detail();
        }
    }
}
