﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BuildItFaster.Models.Buildings;
using BuildItFaster.Models.Resources;

namespace BuildItFaster.Models.AbstractFactoryDP
{
    internal class MineBuildingFactory : IBuildingFactory
    {
        public IBuilding CreateLvl1Building()
        {
            return new MineBuilding(20, 10_000);
        } 

        public IResource? CreateInputResource()
        {
            return null;
        }

        public IResource CreateOutputResource()
        {
            return new IronMine();
        }
    }
}
