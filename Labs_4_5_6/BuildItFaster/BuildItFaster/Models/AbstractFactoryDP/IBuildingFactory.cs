﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BuildItFaster.Models.Buildings;
using BuildItFaster.Models.Resources;

namespace BuildItFaster.Models.AbstractFactoryDP
{
    public interface IBuildingFactory
    {
        public IBuilding CreateLvl1Building();
        public IResource? CreateInputResource();
        public IResource CreateOutputResource();
    }
}
