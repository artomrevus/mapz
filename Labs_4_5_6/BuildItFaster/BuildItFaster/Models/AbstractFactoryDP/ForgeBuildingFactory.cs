﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BuildItFaster.Models.Buildings;
using BuildItFaster.Models.Resources;

namespace BuildItFaster.Models.AbstractFactoryDP
{
    internal class ForgeBuildingFactory : IBuildingFactory
    {
        public IBuilding CreateLvl1Building()
        {
            return new ForgeBuilding(0, 30, 9, 8_000);
        }

        public IResource? CreateInputResource()
        {
            return new IronMine();
        }

        public IResource CreateOutputResource()
        {
            return new Metal();
        }
    }
}
