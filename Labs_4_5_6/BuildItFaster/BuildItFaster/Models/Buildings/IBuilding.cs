﻿using BuildItFaster.Models.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.Buildings
{
    public interface IBuilding
    {
        public string Name { get; }

        public IResource? InputResourceType { get; }
        public uint? InputResourcesInBuilding { get; set; }
        public uint? RequiredInputResourcesCnt { get; }

        public IResource OutputResourceType { get; }
        public uint OutputResourcesCnt { get; }

        public uint ProducingTimeMiliseconds { get; }

        public uint CurrentLevel { get; }
        

        public void Produce();
        public void LevelUp();
    }
}
