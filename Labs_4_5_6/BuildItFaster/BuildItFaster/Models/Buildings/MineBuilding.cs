﻿using System.Threading;
using System;
using BuildItFaster.Models.Resources;
using System.Threading.Tasks;

namespace BuildItFaster.Models.Buildings
{
    internal class MineBuilding : IBuilding
    {
        public MineBuilding(uint outputResourcesCnt, uint producingTimeMiliseconds)
        {
            Name = "Mine";

            InputResourceType = null;
            InputResourcesInBuilding = null;
            RequiredInputResourcesCnt = null;

            OutputResourceType = new IronMine();
            OutputResourcesCnt = outputResourcesCnt;

            ProducingTimeMiliseconds = producingTimeMiliseconds;
            
            CurrentLevel = 1;
        }

        public IResource? InputResourceType { get; }
        public uint? InputResourcesInBuilding { get; set; }
        public uint? RequiredInputResourcesCnt { get; private set; }

        public IResource OutputResourceType { get; }
        public uint OutputResourcesCnt { get; private set; }

        public uint ProducingTimeMiliseconds { get; private set; }

        public uint CurrentLevel { get; private set; }
        public string Name { get; }

       

        

        public void LevelUp()
        {
            CurrentLevel++;

            // To implement 
            throw new NotImplementedException();
        }

        public void Produce()
        {
            if (InputResourcesInBuilding < RequiredInputResourcesCnt)
            {
                throw new InvalidOperationException();
            }

            Thread.Sleep((int)ProducingTimeMiliseconds);

            InputResourcesInBuilding -= RequiredInputResourcesCnt;
        }
    }
}