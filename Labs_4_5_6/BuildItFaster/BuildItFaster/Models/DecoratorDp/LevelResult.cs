﻿using BuildItFaster.Models.SingletonDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.DecoratorDp
{
    internal class LevelResult : ILevelResult
    {
        private LevelData _levelData;

        public LevelResult(LevelData levelData)
        {
            _levelData = levelData;
        }

        public override uint GetPointsCount()
        {
            if (_levelData.IsLevelContinuing)
            {
                throw new InvalidOperationException("Can't perform this action while level is continuing!");
            }

            uint pointsCount = (uint)_levelData.IronMineCurrCnt + (uint)_levelData.MetalCurrCnt * 5 + (uint)_levelData.DetailsCurrCnt * 10;

            if (!_levelData.IsAllResourcesReachedRequiredCnt())
            {
                pointsCount /= 10;
            }

            return pointsCount;
        }

        public override void AddEarnedPointsToUserTotal()
        {
            CurrentUser.Instance.TotalPoints += GetPointsCount();
        }

        public override string GetResultMessage()
        {
            string winOrLostMessage = "";
            if (_levelData.IsAllResourcesReachedRequiredCnt())
            {
                winOrLostMessage = "win =)";
            }
            else
            {
                winOrLostMessage = "lost =(";
            }

            return $"Level finished.\n\nYou {winOrLostMessage}\n\nPoints earned: " + GetPointsCount();
        }
    }
}
