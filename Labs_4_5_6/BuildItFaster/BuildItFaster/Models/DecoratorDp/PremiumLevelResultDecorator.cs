﻿using BuildItFaster.Models.SingletonDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.DecoratorDp
{
    public class PremiumLevelResultDecorator : LevelResultDecorator
    {
        public PremiumLevelResultDecorator(ILevelResult levelResult) : base(levelResult)
        {
        }

        public override string GetResultMessage()
        {
            return base.GetResultMessage() + "\n+\nPremium bonus: " + GetBonusPremiumPoints();
        }

        public override void AddEarnedPointsToUserTotal()
        {
            // main points
            base.AddEarnedPointsToUserTotal();

            // bonus
            CurrentUser.Instance.TotalPoints += GetBonusPremiumPoints();
        }

        private uint GetBonusPremiumPoints()
        {
            return base.GetPointsCount() * 2;
        }
    }
}
