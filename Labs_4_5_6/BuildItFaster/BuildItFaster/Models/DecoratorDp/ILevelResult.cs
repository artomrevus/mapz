﻿namespace BuildItFaster.Models.DecoratorDp
{
    public abstract class ILevelResult
    {
        public abstract uint GetPointsCount();
        public abstract void AddEarnedPointsToUserTotal();
        public abstract string GetResultMessage();
    }
}