﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.DecoratorDp
{
    public class LevelResultDecorator : ILevelResult
    {
        private ILevelResult _levelResult;

        public LevelResultDecorator(ILevelResult levelResult)
        {
            _levelResult = levelResult;
        }

        public override void AddEarnedPointsToUserTotal()
        {
            _levelResult.AddEarnedPointsToUserTotal();
        }

        public override uint GetPointsCount()
        {
            return _levelResult.GetPointsCount();
        }

        public override string GetResultMessage()
        {
            return _levelResult.GetResultMessage();
        }
    }
}
