﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.StrategyDP
{
    class GetBuildingImageContext
    {
        private IGetBuildingImageStrategy? _getBuildingImageStrategy;

        public void SetStrategy(IGetBuildingImageStrategy getBuildingImageStrategy)
        {
            _getBuildingImageStrategy = getBuildingImageStrategy;
        }

        public string? GetBuildingImage()
        {
            return _getBuildingImageStrategy?.GetBuildingImage();
        }
    }
}
