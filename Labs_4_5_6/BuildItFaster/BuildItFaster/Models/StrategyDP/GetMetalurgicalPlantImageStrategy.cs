﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.StrategyDP
{
    internal class GetMetalurgicalPlantImageStrategy : IGetBuildingImageStrategy
    {
        public string GetBuildingImage()
        {
            return "\\\\Mac\\Home\\Documents\\BuildItFaster\\BuildItFaster\\Resources\\Images\\Buildings\\MetalurgicalPlant.png";
        }
    }
}
