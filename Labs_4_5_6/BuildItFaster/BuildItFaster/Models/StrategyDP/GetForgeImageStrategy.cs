﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuildItFaster.Models.StrategyDP
{
    internal class GetForgeImageStrategy : IGetBuildingImageStrategy
    {
        public string GetBuildingImage()
        {
            return "\\\\Mac\\Home\\Documents\\BuildItFaster\\BuildItFaster\\Resources\\Images\\Buildings\\ForgeBuilding.png";
        }
    }
}
