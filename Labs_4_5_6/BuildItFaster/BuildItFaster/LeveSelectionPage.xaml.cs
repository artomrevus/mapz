﻿using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.BuilderDP;
using BuildItFaster.Models.BuilderDP.TemplateMethodDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuildItFaster
{
    /// <summary>
    /// Interaction logic for LeveSelectionPage.xaml
    /// </summary>
    public partial class LevelSelectionPage : Page
    {
        public LevelSelectionPage()
        {
            InitializeComponent();
        }

        private void btnSimpleLevel_Click(object sender, RoutedEventArgs e)
        {
            ILevelBuilder levelBuilder = new LevelBuilder();

            AbstractLevelBuildDirector levelBuildDirector = new SimpleLevelBuildDirector(levelBuilder, new MineBuildingFactory());

            levelBuildDirector.BuildLevel();

            this.NavigationService.Navigate(new LevelPage(levelBuilder.GetBuildingResult()));
        }

        private void btnMediumLevel_Click(object sender, RoutedEventArgs e)
        {
            ILevelBuilder levelBuilder = new LevelBuilder();

            AbstractLevelBuildDirector levelBuildDirector = new MediumLevelBuildDirector(levelBuilder, new MineBuildingFactory());

            levelBuildDirector.BuildLevel();

            this.NavigationService.Navigate(new LevelPage(levelBuilder.GetBuildingResult()));
        }

        private void btnHardLevel_Click(object sender, RoutedEventArgs e)
        {
            ILevelBuilder levelBuilder = new LevelBuilder();

            AbstractLevelBuildDirector levelBuildDirector = new HardLevelBuildDirector(levelBuilder, new MineBuildingFactory());

            levelBuildDirector.BuildLevel();

            this.NavigationService.Navigate(new LevelPage(levelBuilder.GetBuildingResult()));
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StartPage());
        }
    }
}
