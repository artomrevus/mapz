﻿using BuildItFaster.Models.BuilderDP;
using BuildItFaster.Models.SingletonDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuildItFaster
{
    /// <summary>
    /// Interaction logic for CurrentUserPage.xaml
    /// </summary>
    public partial class CurrentUserPage : Page
    {
        private CurrentUser _currentUser;

        public CurrentUserPage(CurrentUser currentUser)
        {
            InitializeComponent();

            _currentUser = currentUser;

            Loaded += LevelPage_Loaded;
        }

        private void LevelPage_Loaded(object sender, RoutedEventArgs e)
        {
            CurrentUserPointsUI.Text = _currentUser.TotalPoints.ToString();
            CurrentUserNameUI.Text = _currentUser.Name ?? CurrentUserNameUI.Text;
            CurrentUserEmailUI.Text = _currentUser.Email ?? CurrentUserEmailUI.Text;

            if (_currentUser.IsHasPremium)
            {
                BuyPremiumButtonUI.Visibility = Visibility.Hidden;
                PremiumUserTextBlockUI.Visibility = Visibility.Visible;
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StartPage());
        }

        private void btnChangePassword_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnBuyPremium_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new BuyPremiumPage());
        }
    }
}
