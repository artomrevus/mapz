﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BuildItFaster.Models;
using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;
using BuildItFaster.Models.FacadeDP;
using BuildItFaster.Models.ObserverDP;
using Microsoft.VisualBasic;
using static System.Net.Mime.MediaTypeNames;
using Image = System.Windows.Controls.Image;

namespace BuildItFaster
{
    /// <summary>
    /// Interaction logic for LevelPage.xaml
    /// </summary>
    public partial class LevelPage : Page, IParameterlessSubscriber
    {
        private readonly LevelData _levelData;
        private BuildingsWithButtonsLinker _buildingsWithButtonsLinker;
        private BuildingInteractionFacade _buildingInteractionFacade;


        public LevelPage(LevelData levelData)
        {
            InitializeComponent();

            _levelData = levelData;

            // ------------------------
            // Observer
            // ------------------------
            _levelData.Subscribe(this);
            // ------------------------

            _levelData.IsLevelContinuing = true;
            DataContext = _levelData;

            _buildingInteractionFacade = new BuildingInteractionFacade();

            Loaded += LevelPage_Loaded;
        }


        private void LevelPage_Loaded(object sender, RoutedEventArgs e)
        {
            _buildingsWithButtonsLinker = ConstructBuildingsWithButtonsLinker();

            MineRequiredCntUI.Text = _levelData.IronMineRequiredCnt.ToString();
            MetalRequiredCntUI.Text = _levelData.MetalRequiredCnt.ToString();
            DetailsRequiredCntUI.Text = _levelData.DetailsRequiredCnt.ToString();
            
            BackgroundImageBrushUI.ImageSource = new BitmapImage(new Uri(_levelData.BackgroundImg, UriKind.RelativeOrAbsolute));
               

            SetBuildingImages();

            if (_levelData.Buildings is not null)
            {
                foreach (var building in _levelData.Buildings)
                {
                    if (building is not null)
                    {
                        _buildingInteractionFacade.ProduceResources(building, _levelData);
                        _buildingsWithButtonsLinker.GetBuildingImageByBuilding(building).MouseLeftButtonDown -= imgBuilding_MouseLeftButtonDown;
                    }
                }
            }
        }


        // -------------------------------------------------------------------
        // Observer
        // -------------------------------------------------------------------
        public void InvokeMethod()
        {
            this.NavigationService.Navigate(new LevelResultsPage(_levelData));
        }
        // -------------------------------------------------------------------

        public void SetBuildingImages()
        {
            foreach (var building in _levelData.Buildings)
            {
                if (building is null)
                {
                    continue;
                }

                string buildigImagePath = "";

                if (building is MineBuilding)
                {
                    buildigImagePath = "\\\\Mac\\Home\\Documents\\BuildItFaster\\BuildItFaster\\Resources\\Images\\Buildings\\MineBuilding.png";
                }
                else if (building is ForgeBuilding)
                {
                    buildigImagePath = "\\\\Mac\\Home\\Documents\\BuildItFaster\\BuildItFaster\\Resources\\Images\\Buildings\\ForgeBuilding.png";
                }
                else if (building is MetalurgicalPlantBuilding)
                {
                    buildigImagePath = "\\\\Mac\\Home\\Documents\\BuildItFaster\\BuildItFaster\\Resources\\Images\\Buildings\\MetalurgicalPlant.png";
                }

                var buildingImage = _buildingsWithButtonsLinker.GetBuildingImageByBuilding(building);
                buildingImage.Source = new BitmapImage(new Uri(buildigImagePath, UriKind.RelativeOrAbsolute));
            }
        }

        public BuildingsWithButtonsLinker ConstructBuildingsWithButtonsLinker()
        {
            var buildingImages = FindVisualChildren<Image>(this)
                .Where(image => image.Style != null && image.Style == (Style)Resources["BuildingImage"])
                .ToList();

            var infoButtons = FindVisualChildren<Button>(this)
                .Where(btton => btton.Style != null && btton.Style == (Style)Resources["InfoButton"])
                .ToList();

            var supplyButtons = FindVisualChildren<Button>(this)
                .Where(btton => btton.Style != null && btton.Style == (Style)Resources["SupplyButton"])
                .ToList();

            var lvlUpButtons = FindVisualChildren<Button>(this)
                .Where(btton => btton.Style != null && btton.Style == (Style)Resources["LvlUpButton"])
                .ToList();

            return new BuildingsWithButtonsLinker(_levelData.Buildings, buildingImages, infoButtons, supplyButtons, lvlUpButtons);
        }

        private static IEnumerable<T> FindVisualChildren<T>(DependencyObject parent) where T : DependencyObject
        {
            if (parent != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }


        private void btnInfo_Click(object sender, RoutedEventArgs e)
        {
            Button? infoButton = sender as Button;

            if (infoButton is null) 
            {
                throw new ArgumentException("Sender must be a Button type");
            }

            MessageBox.Show(
                _buildingInteractionFacade.GetBuildingInfo(_buildingsWithButtonsLinker.GetBuildingByInfoButton(infoButton)!),
                _buildingInteractionFacade.GetBuildingNameAndLvl(_buildingsWithButtonsLinker.GetBuildingByInfoButton(infoButton)!),
                MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        private void btnSurrender_Click(object sender, RoutedEventArgs e)
        {
            _levelData.IsLevelContinuing = false;
            this.NavigationService.Navigate(new LevelResultsPage(_levelData));
        }

        private void btnSupply_Click(object sender, RoutedEventArgs e)
        {
            Button? supplyButton = sender as Button;

            if (supplyButton is null)
            {
                throw new ArgumentException("Sender must be a Button type");
            }

            var suppliedResourcesCnt = GetSuppliedResourcesCntFromUser();
            if (suppliedResourcesCnt is not null)
            {
                try
                {
                    _buildingInteractionFacade.SupplyInputResources(
                        _buildingsWithButtonsLinker.GetBuildingBySupplyButton(supplyButton)!,
                        (uint)suppliedResourcesCnt, _levelData);
                }
                catch(InvalidOperationException ex) 
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private uint? GetSuppliedResourcesCntFromUser()
        {
            string suppliedResourcesCntStr = Microsoft.VisualBasic.Interaction.InputBox("Enter supplied resources count:", "Resources count input", "");

            if (uint.TryParse(suppliedResourcesCntStr, out uint suppliedResourcesCnt))
            {
                return suppliedResourcesCnt;
            }
            else
            {
                MessageBox.Show("The data entered is incorrect. Please try again!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
        }


        private void btnLvlUp_Click(object sender, RoutedEventArgs e)
        {

        }

        private void imgBuilding_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Image? buildingImage = sender as Image;

            if(buildingImage is null)
            {
                return;
            }

            IBuilding? newBuilding = GetNewBuildingFromUser();

            if (newBuilding is not null)
            {
                _buildingsWithButtonsLinker.SetBuildingByBuildingImage(buildingImage, newBuilding);
                _buildingInteractionFacade.ProduceResources(newBuilding, _levelData);

                buildingImage.MouseLeftButtonDown -= imgBuilding_MouseLeftButtonDown;
            }
        }


        private IBuilding? GetNewBuildingFromUser()
        {
            string buildingAsString = Microsoft.VisualBasic.Interaction.InputBox("1 - Iron mine\n2 - Forge\n3 - Metalurgical plant", "Enter building number", "");

            if (uint.TryParse(buildingAsString, out uint buildingAsInt) && buildingAsInt >= 1 && buildingAsInt <=3)
            {
                if(buildingAsInt == 1)
                {
                    return new MineBuildingFactory().CreateLvl1Building();
                }
                else if(buildingAsInt == 2)
                {
                    return new ForgeBuildingFactory().CreateLvl1Building();
                }
                else if(buildingAsInt == 3)
                {
                    return new MetalurgicalPlantBuildingFactory().CreateLvl1Building();
                }
            }

            MessageBox.Show("The data entered is incorrect. Please try again!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            return null;
        }
    }  
}
