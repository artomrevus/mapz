﻿using BuildItFaster.Models;
using BuildItFaster.Models.DecoratorDp;
using BuildItFaster.Models.SingletonDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuildItFaster
{
    /// <summary>
    /// Interaction logic for LevelResultsPage.xaml
    /// </summary>
    public partial class LevelResultsPage : Page
    {
        private LevelData _levelData;
        private ILevelResult _levelResult;

        public LevelResultsPage(LevelData levelData)
        {
            InitializeComponent();

            _levelData = levelData;
            _levelResult = new LevelResult(_levelData);

            if (CurrentUser.Instance.IsHasPremium)
            {
                _levelResult = new PremiumLevelResultDecorator(_levelResult);
            }

            Loaded += LevelResultsPage_Loaded;
        }

        private void LevelResultsPage_Loaded(object sender, RoutedEventArgs e)
        {
            ResultTextBlockUI.Text = _levelResult.GetResultMessage();
            _levelResult.AddEarnedPointsToUserTotal();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StartPage());
        }
    }
}
