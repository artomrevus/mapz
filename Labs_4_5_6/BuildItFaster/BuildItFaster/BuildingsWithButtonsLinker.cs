﻿using BuildItFaster.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using BuildItFaster.Models.AbstractFactoryDP;
using BuildItFaster.Models.Buildings;
using System.Reflection;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using BuildItFaster.Models.StrategyDP;

namespace BuildItFaster
{
    public class BuildingsWithButtonsLinker
    {
        private readonly IList<IBuilding?> _buildings;
        private readonly IList<Image> _buildingImages;
        private readonly IList<Button> _infoButtons;
        private readonly IList<Button> _supplyButtons;
        private readonly IList<Button> _lvlUpButtons;


        public BuildingsWithButtonsLinker(IList<IBuilding?> buildings, IList<Image> buildingImages, IList<Button> infoButtons, 
            IList<Button> supplyButtons, IList<Button> lvlUpButtons)
        {
            if(buildings.Count != buildingImages.Count || buildings.Count != infoButtons.Count || 
                buildings.Count != supplyButtons.Count || buildings.Count != lvlUpButtons.Count)
            {
                throw new ArgumentException("Lists must be the same length!");
            }

            _buildings = buildings;
            _buildingImages = buildingImages;
            _infoButtons = infoButtons;
            _supplyButtons = supplyButtons;
            _lvlUpButtons = lvlUpButtons;
        }

        public Image GetBuildingImageByBuilding(IBuilding building)
        {
            int buildingIndex = _buildings.IndexOf(building);

            if(buildingIndex < 0)
            {
                throw new ArgumentException($"{building} not found!");
            }

            return _buildingImages[buildingIndex];
        }

        public IBuilding? GetBuildingByInfoButton(Button infoButton)
        {
            int buttonIndex = _infoButtons.IndexOf(infoButton);

            if (buttonIndex < 0)
            {
                throw new ArgumentException($"{infoButton} not found!");
            }

            if(buttonIndex >= _buildings.Count)
            {
                throw new InvalidOperationException($"Building do not exist!");
            }

            return _buildings[buttonIndex];
        }

        public IBuilding? GetBuildingBySupplyButton(Button supplyButton)
        {
            int buttonIndex = _supplyButtons.IndexOf(supplyButton);

            if (buttonIndex < 0)
            {
                throw new ArgumentException($"{supplyButton} not found!");
            }

            if (buttonIndex >= _buildings.Count)
            {
                throw new InvalidOperationException($"Building do not exist!");
            }

            return _buildings[buttonIndex];
        }

        public IBuilding? GetBuildingByCollectButton(Button lvlUpButton)
        {
            int buttonIndex = _lvlUpButtons.IndexOf(lvlUpButton);

            if (buttonIndex < 0)
            {
                throw new ArgumentException($"{lvlUpButton} not found!");
            }

            if (buttonIndex >= _buildings.Count)
            {
                throw new InvalidOperationException($"Building do not exist!");
            }

            return _buildings[buttonIndex];
        }

        public void SetBuildingByBuildingImage(Image buildingImage, IBuilding building)
        {
            int imageIndex = _buildingImages.IndexOf(buildingImage);

            if (imageIndex < 0)
            {
                throw new ArgumentException($"{buildingImage} not found!");
            }

            if (imageIndex >= _buildings.Count)
            {
                throw new InvalidOperationException($"Building do not exist!");
            }

            _buildings[imageIndex] = building;


            // ------------------------------------------------------------------------------------------
            // Get building image Strategy DP
            // ------------------------------------------------------------------------------------------

            GetBuildingImageContext getBuildingImageContext = new GetBuildingImageContext();

            if (building is MineBuilding)
            {
                getBuildingImageContext.SetStrategy(new GetMineImageStrategy());
            }
            else if (building is ForgeBuilding)
            {
                getBuildingImageContext.SetStrategy(new GetForgeImageStrategy());
            }
            else if (building is MetalurgicalPlantBuilding)
            {
                getBuildingImageContext.SetStrategy(new GetMetalurgicalPlantImageStrategy());
            }

            string? buildigImagePath = getBuildingImageContext.GetBuildingImage();

            // ------------------------------------------------------------------------------------------
            
            if(buildigImagePath is null)
            {
                return;
            }

            buildingImage.Source = new BitmapImage(new Uri(buildigImagePath, UriKind.RelativeOrAbsolute));
        }
    }
}
