﻿using BuildItFaster.Models.SingletonDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuildItFaster
{
    /// <summary>
    /// Interaction logic for BuyPremiumPage.xaml
    /// </summary>
    public partial class BuyPremiumPage : Page
    {
        public BuyPremiumPage()
        {
            InitializeComponent();
        }

        private void btnBuyIn1Click_Click(object sender, RoutedEventArgs e)
        {
            CurrentUser.Instance.IsHasPremium = true;
            this.NavigationService.Navigate(new CurrentUserPage(CurrentUser.Instance));
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new CurrentUserPage(CurrentUser.Instance));
        }
    }
}
