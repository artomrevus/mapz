﻿using BuildItFaster.Models.SingletonDP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BuildItFaster
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Navigate(new StartPage());

            // For testing (to change in future)
            CurrentUser currentUser = CurrentUser.Instance;
            currentUser.Name = "Artem";
            currentUser.Email = "artomrevus@gmail.com";
            currentUser.Password = "1234678";
            currentUser.TotalPoints = 0;
            currentUser.IsHasPremium = false;
        }
    }
}
