﻿public class BuildingType
{
    public BuildingType(
        int id,
        string? name,
        ResourceType inputResource,
        ResourceType outputResource)
    {
        Id = id;
        Name = name;
        InputResource = inputResource;
        OutputResource = outputResource;
    }

    public int Id { get; init; }
    public string? Name { get; set; }
    public ResourceType InputResource { get; init; }
    public ResourceType OutputResource { get; init; }


    public override bool Equals(object? obj)
    {
        return obj is BuildingType type &&
               Id == type.Id &&
               Name == type.Name &&
               InputResource == type.InputResource &&
               OutputResource == type.OutputResource;
    }


    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Name, InputResource, OutputResource);
    }


    public override string ToString() =>
        String.Format("\"{0}\" building type. Id = {1}. Input resourse - {2}. Output resourse - {3}.", Name, Id, InputResource, OutputResource);
}
