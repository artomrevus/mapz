﻿using System.Xml.Linq;

public class Building
{
    public Building(
        int id,
        int buildingTypeId,
        string? buildingName,
        int inputResourcesPerCycle,
        int outputResourcesPerCycle,
        int cycleTimeSeconds)
    {
        Id = id;
        BuildingTypeId = buildingTypeId;
        BuildingName = buildingName;
        InputResourcesPerCycle = inputResourcesPerCycle;
        OutputResourcesPerCycle = outputResourcesPerCycle;
        CycleTimeSeconds = cycleTimeSeconds;
    }

    public int Id { get; init; }
    public int BuildingTypeId { get; init; }
    public string? BuildingName { get; set; }

    public int InputResourcesPerCycle { get; init; }
    public int OutputResourcesPerCycle { get; init; }
    public int CycleTimeSeconds { get; init; }



    public override bool Equals(object? obj) =>
         obj is Building building &&
         Id == building.Id &&
         BuildingTypeId == building.BuildingTypeId &&
         BuildingName == building.BuildingName &&
         InputResourcesPerCycle == building.InputResourcesPerCycle &&
         OutputResourcesPerCycle == building.OutputResourcesPerCycle &&
         CycleTimeSeconds == building.CycleTimeSeconds;
    

    public override int GetHashCode() =>
         HashCode.Combine(Id, BuildingTypeId, BuildingName, InputResourcesPerCycle, OutputResourcesPerCycle, CycleTimeSeconds);


    public override string ToString() =>
        String.Format("\"{0}\" building. Building id = {1}. Building type id = {2}. Input resourse per cycle: {3}. Output resourse per cycle: {4}. Cycle duration = {5} s.",
            BuildingName, Id, BuildingTypeId, InputResourcesPerCycle, OutputResourcesPerCycle, CycleTimeSeconds);
}
