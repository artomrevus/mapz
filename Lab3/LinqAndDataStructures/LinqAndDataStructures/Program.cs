﻿using System.Resources;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("LinqAndDataStructuresTests")]

internal class Program
{
    private static void Main(string[] args)
    {
        MockObject mockObject = new MockObject();


        // All building types output
        {
            Console.WriteLine("All building types:" + Environment.NewLine);
            foreach (var buildingType in mockObject.BuildingTypes)
            {
                Console.WriteLine(buildingType.ToString() + Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }

        // All buildings output
        {
            Console.WriteLine("All buildings:" + Environment.NewLine);
            foreach (var building in mockObject.Buildings)
            {
                Console.WriteLine(building.ToString() + Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }

        // Dictionary:
        {
            Dictionary<int, List<Building>> buildingsByBuildingTypes = new Dictionary<int, List<Building>>();
            foreach (var building in mockObject.Buildings)
            {
                if (!buildingsByBuildingTypes.ContainsKey(building.BuildingTypeId))
                {
                    buildingsByBuildingTypes[building.BuildingTypeId] = new List<Building>();
                }

                buildingsByBuildingTypes[building.BuildingTypeId].Add(building);
            }
        }


        // LinkedList:
        {
            LinkedList<Building> buildingsLinkedList = new LinkedList<Building>(mockObject.Buildings);
            buildingsLinkedList.AddFirst(new LinkedListNode<Building>(new Building(11, 1, "First LinkedList building", 0, 10, 100)));
            buildingsLinkedList.AddLast(new LinkedListNode<Building>(new Building(12, 3, "Last LinkedList building", 5, 10, 40)));

            Console.WriteLine("All buildings in LinkedList:" + Environment.NewLine);
            foreach (var building in buildingsLinkedList)
            {
                Console.WriteLine(building.ToString() + Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }

        // SortedList:
        {
            SortedList<int, Building> buildingsSortedList = new SortedList<int, Building>();
            buildingsSortedList.Add(mockObject.Buildings[9].BuildingTypeId, mockObject.Buildings[9]);
            buildingsSortedList.Add(mockObject.Buildings[4].BuildingTypeId, mockObject.Buildings[4]);
            buildingsSortedList.Add(mockObject.Buildings[1].BuildingTypeId, mockObject.Buildings[1]);

            Console.WriteLine("All buildings in SortedList:" + Environment.NewLine);
            foreach (var building in buildingsSortedList)
            {
                Console.WriteLine(building.Value.ToString() + Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }

        // Queue:
        {
            Queue<Building> buildingsQueue = new Queue<Building>(mockObject.Buildings);
            buildingsQueue.Enqueue(new Building(11, 1, "Last added Queue building", 0, 10, 100));

            Console.WriteLine("All buildings in Queue:" + Environment.NewLine);
            while (buildingsQueue.Count != 0)
            {
                Console.WriteLine(buildingsQueue.Dequeue().ToString() + Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }

        // Stack:
        {
            Stack<Building> buildingsStack = new Stack<Building>(mockObject.Buildings);
            buildingsStack.Push(new Building(11, 1, "Last added Stack building", 0, 10, 100));

            Console.WriteLine("All buildings in Stack:" + Environment.NewLine);
            while (buildingsStack.Count != 0)
            {
                Console.WriteLine(buildingsStack.Pop().ToString() + Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }


        // HashSet:
        {
            HashSet<Building> buildingsHashSet = new HashSet<Building>(mockObject.Buildings);
            buildingsHashSet.Add(new Building(11, 1, "New HashSet building", 0, 10, 100));
            buildingsHashSet.Add(new Building(11, 1, "New HashSet building", 0, 10, 100));

            Console.WriteLine("All buildings in HashSet:" + Environment.NewLine);
            foreach (var building in buildingsHashSet)
            {
                Console.WriteLine(building.ToString() + Environment.NewLine);
            }
            Console.WriteLine(Environment.NewLine);
        }


        Console.ReadKey();
    }
}
