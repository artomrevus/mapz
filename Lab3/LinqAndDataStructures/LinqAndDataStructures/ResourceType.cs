﻿public enum ResourceType
{
     NotRequired = 0,
     GoldMine,
     GoldBar,
     GoldJewellery,
     Money
}

/* public enum BuildingType
{
    Mine = 1,
    Forge,
    JewelleryFactory,
    JewelleryStore
}
*/