﻿using System;

public class CycleTimeComparer : IComparer<int>
{
    public int Compare(int cycleTime1, int cycleTime2)
    {
        return cycleTime1.CompareTo(cycleTime2);
    }
}


