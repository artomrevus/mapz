﻿using System;

public static class ResourceTypeExtentions
{
	public static string ConvertToString(this ResourceType resourceType)
	{
		return $"ResourceType: {resourceType}. As number: {(int)resourceType}";
	}

	public static bool ІsPossibleDirectlyConvertTo(this ResourceType resourceType1, ResourceType resourceType2)
	{
		return (int)resourceType1 == (int)resourceType2 - 1 ? true : false;
	}
}


