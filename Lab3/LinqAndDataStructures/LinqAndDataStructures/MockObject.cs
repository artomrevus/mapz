﻿public class MockObject
{
    public List<Building> Buildings
    {
        get
        {
            return new List<Building>
            {
                new Building(
                 id: 1,
                 buildingTypeId: 1,
                 buildingName: "Gold mine. Lvl 1. #1",
                 inputResourcesPerCycle: 0,
                 outputResourcesPerCycle: 2,
                 cycleTimeSeconds: 30
                ),

                new Building(
                 id: 2,
                 buildingTypeId: 1,
                 buildingName: "Gold mine. Lvl 2. #1",
                 inputResourcesPerCycle: 0,
                 outputResourcesPerCycle: 3,
                 cycleTimeSeconds: 25
                ),

                new Building(
                 id: 3,
                 buildingTypeId: 1,
                 buildingName: "Gold mine. Lvl 2. #2",
                 inputResourcesPerCycle: 0,
                 outputResourcesPerCycle: 3,
                 cycleTimeSeconds: 25
                ),

                new Building(
                 id: 4,
                 buildingTypeId: 2,
                 buildingName: "Forge. Lvl 2. #1",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 3,
                 cycleTimeSeconds: 40
                ),

                new Building(
                 id: 5,
                 buildingTypeId: 3,
                 buildingName: "Jewellery factory. Lvl 1. #1",
                 inputResourcesPerCycle: 10,
                 outputResourcesPerCycle: 1,
                 cycleTimeSeconds: 50
                ),

                new Building(
                 id: 6,
                 buildingTypeId: 3,
                 buildingName: "Jewellery factory. Lvl 1. #2",
                 inputResourcesPerCycle: 10,
                 outputResourcesPerCycle: 1,
                 cycleTimeSeconds: 50
                ),

                new Building(
                 id: 7,
                 buildingTypeId: 3,
                 buildingName: "Jewellery factory. Lvl 2. #1",
                 inputResourcesPerCycle: 9,
                 outputResourcesPerCycle: 1,
                 cycleTimeSeconds: 45
                ),

                new Building(
                 id: 8,
                 buildingTypeId: 4,
                 buildingName: "Jewellery store. Lvl 1. #1",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 5,
                 cycleTimeSeconds: 70
                ),

                new Building(
                 id: 9,
                 buildingTypeId: 4,
                 buildingName: "Jewellery store. Lvl 2. #1",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 10,
                 cycleTimeSeconds: 60
                ),

                new Building(
                 id: 10,
                 buildingTypeId: 4,
                 buildingName: "Jewellery store. Lvl 2. #2",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 10,
                 cycleTimeSeconds: 60
                )
            };
        }
    }

    public List<BuildingType> BuildingTypes
    {
        get
        {
            return new List<BuildingType>
            {
                new BuildingType(
                 id: 1,
                 name: "Gold mine",
                 inputResource: ResourceType.NotRequired,
                 outputResource: ResourceType.GoldMine
                ),

                new BuildingType(
                 id: 2,
                 name: "Forge",
                 inputResource: ResourceType.GoldMine,
                 outputResource: ResourceType.GoldBar
                ),

                new BuildingType(
                 id: 3,
                 name: "Jewellery factory",
                 inputResource: ResourceType.GoldBar,
                 outputResource: ResourceType.GoldJewellery
                ),

                new BuildingType(
                 id: 4,
                 name: "Jewellery store",
                 inputResource: ResourceType.GoldJewellery,
                 outputResource: ResourceType.Money
                )
            };
         }
    }
}