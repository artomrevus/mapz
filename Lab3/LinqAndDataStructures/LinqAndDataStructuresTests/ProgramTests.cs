﻿using Moq;
using NUnit.Framework;


namespace LinqAndDataStructuresTests;


[TestFixture]
public class LinqDemoTests
{
    MockObject mockObject = new MockObject();


    [Test] 
    public void SelectTest()
    {
        var expectedResult = new List<string>
        {
            "Gold mine. Lvl 1. #1",
            "Gold mine. Lvl 2. #1",
            "Gold mine. Lvl 2. #2",
            "Forge. Lvl 2. #1",
            "Jewellery factory. Lvl 1. #1",
            "Jewellery factory. Lvl 1. #2",
            "Jewellery factory. Lvl 2. #1",
            "Jewellery store. Lvl 1. #1",
            "Jewellery store. Lvl 2. #1",
            "Jewellery store. Lvl 2. #2"
        };

        var actualResult = mockObject.Buildings.Select(building => building.BuildingName);

        CollectionAssert.AreEqual(expectedResult, actualResult);
    }


    [Test]
    public void WhereTest()
    {
        var expectedResult = new List<Building>
        {
            new Building(
             id: 8,
             buildingTypeId: 4,
             buildingName: "Jewellery store. Lvl 1. #1",
             inputResourcesPerCycle: 5,
             outputResourcesPerCycle: 5,
             cycleTimeSeconds: 70
            ),

            new Building(
             id: 9,
             buildingTypeId: 4,
             buildingName: "Jewellery store. Lvl 2. #1",
             inputResourcesPerCycle: 5,
             outputResourcesPerCycle: 10,
             cycleTimeSeconds: 60
            ),

            new Building(
             id: 10,
             buildingTypeId: 4,
             buildingName: "Jewellery store. Lvl 2. #2",
             inputResourcesPerCycle: 5,
             outputResourcesPerCycle: 10,
             cycleTimeSeconds: 60
            )
        };

        var actualResult = mockObject.Buildings.Where(building => building.CycleTimeSeconds >= 60);

        CollectionAssert.AreEqual(expectedResult, actualResult);
    }


    [TestCase(ResourceType.GoldBar, "ResourceType: GoldBar. As number: 2")]
    [TestCase(ResourceType.NotRequired, "ResourceType: NotRequired. As number: 0")]
    public void ConvetToStringExtentionTest(ResourceType resourceType, string expectedResult)
    {
        var actualResult = resourceType.ConvertToString();

        Assert.AreEqual(expectedResult, actualResult);
    }


    [TestCase(ResourceType.GoldMine, ResourceType.GoldBar, true)]
    [TestCase(ResourceType.GoldJewellery, ResourceType.GoldMine, false)]
    public void ІsPossibleDirectlyConvertToExtentionTest(ResourceType resourceType1, ResourceType resourceType2, bool expectedResult)
    {
        var actualResult = resourceType1.ІsPossibleDirectlyConvertTo(resourceType2);

        Assert.AreEqual(expectedResult, actualResult);
    }


    [Test]
    public void AnonymousClassesTest()
    {
        var expectedResult = new List<dynamic>
        {
            new
            {
                Name = "Gold mine. Lvl 1. #1",
                BuildingTypeId = 1,
            },

            new
            {
                Name = "Gold mine. Lvl 2. #1",
                BuildingTypeId = 1,
            },

            new
            {
                Name = "Gold mine. Lvl 2. #2",
                BuildingTypeId = 1,
            },

            new
            {
                Name = "Forge. Lvl 2. #1",
                BuildingTypeId = 2,
            },

            new
            {
                Name = "Jewellery factory. Lvl 1. #1",
                BuildingTypeId = 3,
            },

            new
            {
                Name = "Jewellery factory. Lvl 1. #2",
                BuildingTypeId = 3,
            },

            new
            {
                Name = "Jewellery factory. Lvl 2. #1",
                BuildingTypeId = 3,
            }
        };

        var actualResult = mockObject.Buildings
            .Select(building => new { Name = building.BuildingName, BuildingTypeId = building.BuildingTypeId })
            .Where(buildingMainInfo => buildingMainInfo.BuildingTypeId <= 3);

        CollectionAssert.AreEqual(expectedResult, actualResult);
    }


    [Test]
    public void IcomparerSortTest()
    {
        var expectedResult = new List<dynamic>
        {
                new Building(
                 id: 2,
                 buildingTypeId: 1,
                 buildingName: "Gold mine. Lvl 2. #1",
                 inputResourcesPerCycle: 0,
                 outputResourcesPerCycle: 3,
                 cycleTimeSeconds: 25
                ),

                new Building(
                 id: 3,
                 buildingTypeId: 1,
                 buildingName: "Gold mine. Lvl 2. #2",
                 inputResourcesPerCycle: 0,
                 outputResourcesPerCycle: 3,
                 cycleTimeSeconds: 25
                ),

                new Building(
                 id: 1,
                 buildingTypeId: 1,
                 buildingName: "Gold mine. Lvl 1. #1",
                 inputResourcesPerCycle: 0,
                 outputResourcesPerCycle: 2,
                 cycleTimeSeconds: 30
                ),

                new Building(
                 id: 4,
                 buildingTypeId: 2,
                 buildingName: "Forge. Lvl 2. #1",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 3,
                 cycleTimeSeconds: 40
                ),

                new Building(
                 id: 7,
                 buildingTypeId: 3,
                 buildingName: "Jewellery factory. Lvl 2. #1",
                 inputResourcesPerCycle: 9,
                 outputResourcesPerCycle: 1,
                 cycleTimeSeconds: 45
                ),

                new Building(
                 id: 5,
                 buildingTypeId: 3,
                 buildingName: "Jewellery factory. Lvl 1. #1",
                 inputResourcesPerCycle: 10,
                 outputResourcesPerCycle: 1,
                 cycleTimeSeconds: 50
                ),

                new Building(
                 id: 6,
                 buildingTypeId: 3,
                 buildingName: "Jewellery factory. Lvl 1. #2",
                 inputResourcesPerCycle: 10,
                 outputResourcesPerCycle: 1,
                 cycleTimeSeconds: 50
                ),

                new Building(
                 id: 9,
                 buildingTypeId: 4,
                 buildingName: "Jewellery store. Lvl 2. #1",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 10,
                 cycleTimeSeconds: 60
                ),

                new Building(
                 id: 10,
                 buildingTypeId: 4,
                 buildingName: "Jewellery store. Lvl 2. #2",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 10,
                 cycleTimeSeconds: 60
                ),

                new Building(
                 id: 8,
                 buildingTypeId: 4,
                 buildingName: "Jewellery store. Lvl 1. #1",
                 inputResourcesPerCycle: 5,
                 outputResourcesPerCycle: 5,
                 cycleTimeSeconds: 70
                )
        };

        var actualResult = mockObject.Buildings
            .OrderBy(buildingMainInfo => buildingMainInfo.CycleTimeSeconds, new CycleTimeComparer());

       CollectionAssert.AreEqual(expectedResult, actualResult);
    }


    [Test]
    public void ConvertListToArrayTest()
    {
        var expectedResult = mockObject.Buildings;

        var actualResult = mockObject.Buildings.ToArray();

        CollectionAssert.AreEqual(expectedResult, actualResult);
    }


    [Test]
    public void SortByNameTest()
    {
        var expectedResult = new List<BuildingType>
        {
               new BuildingType(
                 id: 2,
                 name: "Forge",
                 inputResource: ResourceType.GoldMine,
                 outputResource: ResourceType.GoldBar
                ),

               new BuildingType(
                 id: 1,
                 name: "Gold mine",
                 inputResource: ResourceType.NotRequired,
                 outputResource: ResourceType.GoldMine
               ),

               new BuildingType(
                 id: 3,
                 name: "Jewellery factory",
                 inputResource: ResourceType.GoldBar,
                 outputResource: ResourceType.GoldJewellery
                ),

               new BuildingType(
                 id: 4,
                 name: "Jewellery store",
                 inputResource: ResourceType.GoldJewellery,
                 outputResource: ResourceType.Money
               )
        };

        var actualResult = mockObject.BuildingTypes
            .OrderBy(buildingType => buildingType.Name);

        CollectionAssert.AreEqual(expectedResult, actualResult);
    }

}

