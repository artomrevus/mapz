﻿// Implement various access modifiers. Demonstrate access to these modifiers. 

public class ModificatorsClass
{
    public int publicField;
    internal int internalField;
    protected int protectedField;
    private int privateField;
    protected internal int protectedInternalField;
    private protected int privateProtectedField;
}

public class InheritedСlass : ModificatorsClass
{
    public void Method()
    {
        publicField = 0; // access is available
        internalField = 0; // access is available
        protectedField = 0; // access is available
        privateField = 0; // no access
        protectedInternalField = 0; // access is available
        privateProtectedField = 0; // access is available
    }
}

public class NotInheritedСlass
{
    public void Method()
    {
        ModificatorsClass modificatorsClassInstance = new ModificatorsClass();

        modificatorsClassInstance.publicField = 0; // access is available
        modificatorsClassInstance.internalField = 0; // access is available
        modificatorsClassInstance.protectedField = 0; // no access
        modificatorsClassInstance.privateField = 0; // no access
        modificatorsClassInstance.protectedInternalField = 0; // access is available
        modificatorsClassInstance.privateProtectedField = 0; // no access
    }
}

internal class Program
{
    private static void Main(string[] args)
    {

    }
}