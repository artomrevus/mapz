﻿// Implement fields and classes without access modifiers. Investigate what the default access will be for classes, structures, interfaces,
// nested classes, fields, etc. 


class ClassWithoutAccessMofifier 
{

}

struct StructWithoutAccessModifier
{

}

interface InterfaceWithoutAccessModifier
{

}

public class PublicOuterClass
{
    class NestedClass
    {}
}

public class ClassWithFields
{
    int variableWithoutAccessModifier;
    void MethodWithoutAccessModifier() { }
}

internal class InheritedСlass : ClassWithFields
{
    public void Method()
    {
        ClassWithoutAccessMofifier classWithoutAccessMofifier = new ClassWithoutAccessMofifier();
        StructWithoutAccessModifier structWithoutAccessModifier = new StructWithoutAccessModifier();
        InterfaceWithoutAccessModifier interfaceWithoutAccessModifier;
        PublicOuterClass.NestedClass nestedClass = new PublicOuterClass.NestedClass(); // 'PublicOuterClass.NestedClass' is inaccessible due
                                                                                       // to its protection level
        new ClassWithFields().variableWithoutAccessModifier = 0; // ClassWithFields.variableWithoutAccessModifier' is inaccessible due to its protection level
        new ClassWithFields().MethodWithoutAccessModifier(); // 'ClassWithFields.MethodWithoutAccessModifier()' is inaccessible due to its protection level
    }
}

public class NotInheritedСlass
{
    public void Method()
    {
        ClassWithoutAccessMofifier classWithoutAccessMofifier = new ClassWithoutAccessMofifier();
        StructWithoutAccessModifier structWithoutAccessModifier = new StructWithoutAccessModifier();
        InterfaceWithoutAccessModifier interfaceWithoutAccessModifier;
        PublicOuterClass.NestedClass nestedClass = new PublicOuterClass.NestedClass(); // 'PublicOuterClass.NestedClass' is inaccessible due
                                                                                       // to its protection level
        new ClassWithFields().variableWithoutAccessModifier = 0; // ClassWithFields.variableWithoutAccessModifier' is inaccessible due to its protection level
        new ClassWithFields().MethodWithoutAccessModifier(); // 'ClassWithFields.MethodWithoutAccessModifier()' is inaccessible due to its protection level
    }
}