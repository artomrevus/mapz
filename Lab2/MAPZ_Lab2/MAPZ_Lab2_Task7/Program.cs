﻿// Implement overloading of constructors of the base class and the current class.  Show different options for using the base and this keywords.
// Implement method overloading.

internal class Base
{
    private int var1;
    public int Var1 { get => var1; set => var1 = value; }
   
    private int var2;
    public int Var2 { get => var2; set => var2 = value; }

    internal Base()
    {
        var1 = 0;
        var2 = 0;
    }

    internal Base(int var1, int var2)
    {
        this.var1 = var1;
        this.var2 = var2;
    }

    internal void Method() => Console.WriteLine("void Method()");
}

internal class Derived : Base
{
    private int var1;
    public int Var1 { get => var1; set => var1 = value; }

    internal Derived(int baseVar1, int baseVar2, int derivedVar1) : base(baseVar1, baseVar2)
    {
        this.var1 = derivedVar1;
    }

    internal Derived(int baseVar1, int baseVar2) : this(baseVar1, baseVar2, 0)
    {
        base.Var1++;
        base.Var2++;
    }

    internal void Method(int param) => Console.WriteLine("void Method(int)");
}