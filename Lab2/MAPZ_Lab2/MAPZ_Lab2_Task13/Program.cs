﻿// Explore the structures. Implement structure inheritance through interfaces.

// ----------------------------------------------------------------------------------------------------

using System.Drawing;

internal interface ICar
{
    bool Drive();
    bool Stop();
}

internal abstract struct Car : ICar // Error: The modifier 'abstract' is not valid for struct
{

}

internal struct BMW_X6 : Car  // Error: Type 'Car' (struct) in interface list is not an interface
{
    
}

internal struct Program // Program also can be struct (not only class)
{
    private static void Main(string[] args)
    {
        BMW_X6 X6_1 = new BMW_X6();
    }
}





// ----------------------------------------------------------------------------------------------------





public struct ModificatorsClass
{
    public int publicField;
    internal int internalField;
    protected int protectedField; // Error: protected member declared in struct
    private int privateField;
    protected internal int protectedInternalField; // Error: protected member declared in struct
    private protected int privateProtectedField;  // Error: protected member declared in struct
}





// ----------------------------------------------------------------------------------------------------






public struct PublicOuterClass
{
    struct NestedClass  // Struct also can be nested
    {}
}





// ----------------------------------------------------------------------------------------------------





internal interface IElectricalDevice
{
    void On();
    void Off();
}

internal interface IRadioSignalReceiver
{
    void ChangeFrequency(double frequency);
}

internal interface ISoundDevice
{
    void ChangeVolume(uint volume);
}

internal interface IRadio : IElectricalDevice, IRadioSignalReceiver, ISoundDevice
{
    string Model { get; }
    Color Color { get; }
    double CurrentFrequency { get; set; }
    double CurrentVolume { get; set; }
}

internal struct Radio : IRadio, IElectricalDevice, IRadioSignalReceiver, ISoundDevice  // struct can inherit multiple interfaces
{
    public string Model { get; }
    public Color Color { get; }
    public double CurrentFrequency { get; set; }
    public double CurrentVolume { get; set; }

    public Radio(string model, Color color)
    {
        Model = model;
        Color = color;
        CurrentFrequency = 0.0;
        CurrentVolume = 0.0;
    }

    public void ChangeFrequency(double frequency)
    {
        CurrentFrequency = frequency;
    }

    public void ChangeVolume(uint volume)
    {
        CurrentVolume = volume;
    }

    public void Off()
    {
        Console.WriteLine("Silence...");
    }

    public void On()
    {
        Console.WriteLine("♪♫♬");
    }
}





// ----------------------------------------------------------------------------------------------------





internal struct FieldsClass
{
    private static int staticField;
    private int dynamicField;

    static FieldsClass()
    {
        staticField = 10;
        Console.WriteLine("Static constructor");
    }

    internal FieldsClass() // Error: The parameterless struct constructor must be 'public'.
    {
        staticField = 10;
        dynamicField = 100;
        Console.WriteLine("Dynamic constructor");
    }
}





// ----------------------------------------------------------------------------------------------------