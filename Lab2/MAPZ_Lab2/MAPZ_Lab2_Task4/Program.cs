﻿// Declare an inner class with access less than public. Implement a field of this data type. Investigate the restrictions on the modifier.

public class OuterClass
{
    internal class InternalClass
    {}

    public InternalClass publicFieldInternalClass; //  Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field
                                                   //  'OuterClass.publicFieldInternalClass'
    internal InternalClass internalFieldInternalClass;

    protected InternalClass protectedFieldInternalClass; //  Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field
                                                         //  'OuterClass.protectedFieldInternalClass'
    private InternalClass privateFieldInternalClass;

    internal protected InternalClass internalProtectedFieldInternalClass; // Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field 
                                                                          // 'OuterClass.internalProtectedFieldInternalClass'
    private protected InternalClass privateProtectedFieldInternalClass;
 


    protected class ProtectedClass
    {}

    public ProtectedClass publicFieldProtectedClass; //  Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field
                                                     //  'OuterClass.publicFieldProtectedClass'
    internal ProtectedClass internalFieldProtectedClass; //  Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field
                                                         //  'OuterClass.internalFieldProtectedClass'
    protected ProtectedClass protectedFieldProtectedClass;

    private ProtectedClass privateFieldProtectedClass;

    internal protected ProtectedClass internalProtectedFieldProtectedClass; // Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field 
                                                                            // 'OuterClass.internalProtectedFieldProtectedClass'
    private protected ProtectedClass privateProtectedFieldProtectedClass;



    private class PrivateClass
    {}

    public PrivateClass publicFieldPrivateClass; //  Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field
                                                 //  'OuterClass.publicFieldPrivateClass'
    internal PrivateClass internalFieldPrivateClass; //  Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field
                                                     //  'OuterClass.internalFieldPrivateClass'
    protected PrivateClass protectedFieldPrivateClass; //  Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field
                                                       //  'OuterClass.protectedFieldPrivateClass'
    private PrivateClass privateFieldPrivateClass;

    internal protected PrivateClass internalProtectedFieldPrivateClass; // Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field 
                                                                        // 'OuterClass.internalProtectedFieldPrivateClass'
    private protected PrivateClass privateProtectedFieldPrivateClass; // Inconsistent accessibility: field type 'OuterClass.InternalClass' is less accessible than field 
                                                                      // 'OuterClass.privateProtectedFieldPrivateClass'

}