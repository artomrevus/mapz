﻿// Implement explicit and implicit cast operators.

using System.Drawing;

internal class Apple
{
    private double _weight;
    private Color _color;
    private string _variety;


    internal Apple(double weight, Color color, string variety)
    {
        _weight = weight;
        _color = color;
        _variety = variety;
    }

    public static implicit operator Color(Apple apple)
    {
        return apple._color;
    }

    public static explicit operator double(Apple apple)
    {
        return apple._weight;
    }
}

internal class Program
{
    private static void Main(string[] args)
    {
        Apple apple = new Apple(0.12, Color.Red, "Mystery");

        Color appleColor = apple; // implicit cast
        Console.WriteLine(appleColor);

        double appleWeight = (double)apple; // explicit cast
        Console.WriteLine(appleWeight);
    }
}