﻿// Implement the enumerated type. Demonstrate operations on enumerated types (^, ||, &, |, &&, ~, >>, <<).

internal enum DaysOfWeek
{
    Monday = 1,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
}

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine(DaysOfWeek.Saturday ^ DaysOfWeek.Tuesday); // 110 ^ 010 = 100 => Output: Thursday
        Console.WriteLine(DaysOfWeek.Saturday & DaysOfWeek.Tuesday); // 110 ^ 010 = 010 => Output: Tuesday
        Console.WriteLine(DaysOfWeek.Saturday | DaysOfWeek.Tuesday); // 110 ^ 010 = 110 => Output: Saturday
        Console.WriteLine(~DaysOfWeek.Friday); // ~101 => Output: -6
        Console.WriteLine((DaysOfWeek)((int)DaysOfWeek.Saturday >> 1)); // 110 >> 1 = 011 => Output: Wednesday
        Console.WriteLine((DaysOfWeek)((int)DaysOfWeek.Monday << 1)); // 01 << 1 = 10 => Output: Tuesday

        DaysOfWeek day4 = DaysOfWeek.Thursday;
        DaysOfWeek day7 = DaysOfWeek.Sunday;
        Console.WriteLine((day4 == DaysOfWeek.Thursday) && (day7 != DaysOfWeek.Sunday)); // true && false = false => Output: false
        Console.WriteLine((day4 != DaysOfWeek.Thursday) || (day7 > DaysOfWeek.Wednesday)); // false || true = true => Output: true
    }
}