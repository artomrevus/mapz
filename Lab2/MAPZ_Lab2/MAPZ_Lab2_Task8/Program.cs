﻿// Implement different types of initialisation of fields, both static and dynamic: using a static and dynamic constructor.
// Investigate the sequence in which fields are initialised.

internal class FieldsClass
{
    private static int staticField;
    private int dynamicField;

    static FieldsClass()
    {
        staticField = 10;
        Console.WriteLine("Static constructor");
    }

    internal FieldsClass()
    {
        staticField = 10;
        dynamicField = 100;
        Console.WriteLine("Dynamic constructor");
    }
}

internal class Program
{
    private static void Main(string[] args)
    {
        FieldsClass fieldsClass1 = new FieldsClass();
        FieldsClass fieldsClass2 = new FieldsClass();

        // Output:
        // Static constructor
        // Dynamic constructor
        // Dynamic constructor
    }
}