﻿// Implement an inheritance chain that would include a regular class, an abstract class, and an interface.

internal interface ICar
{
    bool Drive();
    bool Stop();
}

internal abstract class Car : ICar
{ 
    public int EngineDisplacement { get; set; }
    public int EnginePower { get; set; }
    public string? FuelType { get; set; }
    public string? DriveType { get; set; }
    public string? Color { get; set; }
    public int SeatsNumber { get; set; }

    protected Car(int engineDisplacement, int EnginePower, string? FuelType, string? DriveType, string? Color, int SeatsNumber)
    {
        throw new NotImplementedException();
    }

    public virtual bool Drive()
    {
        throw new NotImplementedException();
    }

    public virtual bool Stop()
    {
        throw new NotImplementedException();
    }
}

internal class BMW_X6 : Car
{
    public BMW_X6(int engineDisplacement, int EnginePower, string? FuelType, string? DriveType, string? Color, int SeatsNumber)
        : base(engineDisplacement, EnginePower, FuelType, DriveType, Color, SeatsNumber)
    {
        throw new NotImplementedException();
    }

    public override bool Drive()
    {
        throw new NotImplementedException();
    }

    public override bool Stop()
    {
        throw new NotImplementedException();
    }
}

internal class Program
{
    private static void Main(string[] args)
    {
        BMW_X6 X6_1 = new BMW_X6(2998, 340, "Petrol", "Front-wheel", "Black", 4);
    }
}