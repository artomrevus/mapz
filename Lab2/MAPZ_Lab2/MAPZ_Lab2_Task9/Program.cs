﻿// Implement functions with the out, ref parameters. Show the differences with and without these parameters.

internal class Program
{
    static void MethodWithoutOutAndRef(int value)
    {
        value -= 10;
    }

    static void MethodWithOut(out int value)
    {
        // The out parameter 'value' must be assigned
        value = 100;
    }

    static void MethodWithRef(ref int value)
    {
        // The out parameter 'value' must not be assigned
        value += 100;
    }

    private static void Main(string[] args)
    {
        MethodWithOut(out int value);
        Console.WriteLine($"Value: {value}"); // Output: 100

        MethodWithRef(ref value);
        Console.WriteLine($"Value: {value}"); // Output: 200

        MethodWithoutOutAndRef(value);
        Console.WriteLine($"Value: {value}"); // Output: 200
    }
}