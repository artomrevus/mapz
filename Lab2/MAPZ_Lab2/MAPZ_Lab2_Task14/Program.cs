﻿// Override and explore the methods of the object class

internal class Student
{
    public uint Age { get; init; }
    public string? FirstName { get; set; }
    public string? SecondName { get; set; }

    public override string ToString() // converts instance to string
    {
        return $"{FirstName} {SecondName}. Age: {Age}.";
    }

    public override int GetHashCode() // returns hash code of object
    {
        return (FirstName + SecondName).GetHashCode();
    }

    public override bool Equals(object? obj) // checks if this equal obj
    {
        if(!(obj is Student))
        {
            return false;
        }
        else
        {
            Student student = obj as Student;

            if (student is not null)
            {
                return (this.Age == student.Age) && (this.FirstName == student.FirstName)
                    && (this.SecondName == student.SecondName);
            }
            else
            {
                return false;
            }
        }
    }
}

internal class Program
{
    private static void Main(string[] args)
    {
        Student student1 = new Student
        {
            Age = 18,
            FirstName = "Artom",
            SecondName = "Revus"
        };

        Student student2 = new Student
        {
            Age = 19,
            FirstName = "Artom",
            SecondName = "Revus"
        };

        Console.WriteLine(student1);
        Console.WriteLine(student1.GetHashCode());
        Console.WriteLine(student1.Equals(student2));
    }
}