﻿// Consider the speed of methods with different access modifiers private, protected, etc.

using System.Diagnostics;

internal class Program
{
    private static Func<int, double, string, string> ResearchedMethod = (int param1, double param2, string param3) =>
    {
        double res = (double)param1 + param2;
        param3 += res.ToString();
        return param3;
    };

    private static string PrivateMethod(int param1, double param2, string param3)
    {
        return ResearchedMethod(param1, param2, param3);
    }

    protected static string ProtectedMethod(int param1, double param2, string param3)
    {
        return ResearchedMethod(param1, param2, param3);
    }

    internal static string InternalMethod(int param1, double param2, string param3)
    {
        return ResearchedMethod(param1, param2, param3);
    }

    public static string PublicMethod(int param1, double param2, string param3)
    {
        return ResearchedMethod(param1, param2, param3);
    }

    internal protected static string InternalProtectedMethod(int param1, double param2, string param3)
    {
        return ResearchedMethod(param1, param2, param3);
    }

    private protected static string PrivateProtectedMethod(int param1, double param2, string param3)
    {
        return ResearchedMethod(param1, param2, param3);
    }

    internal class Nested
    { 
        internal static string InternalMethod(int param1, double param2, string param3)
        {
            return ResearchedMethod(param1, param2, param3);
        }

        public static string PublicMethod(int param1, double param2, string param3)
        {
            return ResearchedMethod(param1, param2, param3);
        }

        internal protected static string InternalProtectedMethod(int param1, double param2, string param3)
        {
            return ResearchedMethod(param1, param2, param3);
        }
    }


    internal static void ResearchSpeedOfAllMethods(int numberOfCallsEachMethod = 1000000)
    {
        ResearchSpeedOfMethod(PrivateMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(PrivateMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(PrivateMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(PrivateMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(PrivateMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(PrivateMethod, numberOfCallsEachMethod);

        ResearchSpeedOfMethod(PrivateMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(ProtectedMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(InternalMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(PublicMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(InternalProtectedMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(PrivateProtectedMethod, numberOfCallsEachMethod);
    }

    internal static void ResearchSpeedOfAllNestedMethods(int numberOfCallsEachMethod = 1000000)
    {
        Console.WriteLine("Nested class: ");
        ResearchSpeedOfMethod(Nested.InternalMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(Nested.PublicMethod, numberOfCallsEachMethod);
        ResearchSpeedOfMethod(Nested.InternalProtectedMethod, numberOfCallsEachMethod);
    }

    private static void ResearchSpeedOfMethod(Func<int, double, string, string> ResearchedMethod, int numberOfCallsEachMethod)
    {
        //Console.ReadKey();
        Console.WriteLine(ResearchedMethod.Method.Name);

        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        for (int i = 0; i < numberOfCallsEachMethod; ++i)
        {
            ResearchedMethod(5, 8.7, "String parameter");
        }
        stopwatch.Stop();

        Console.WriteLine("Time in milliseconds = " + stopwatch.ElapsedMilliseconds + "ms");
    }

    private static void Main(string[] args)
    {
        ResearchSpeedOfAllMethods(10000000);
        Console.WriteLine();
        ResearchSpeedOfAllNestedMethods(10000000);
    }
}