﻿public class InheritedСlass : ModificatorsClass
{
    public void Method()
    {
        publicField = 0; // access is available
        internalField = 0; // no access
        protectedField = 0; // access is available
        privateField = 0; // no access
        protectedInternalField = 0; // access is available
        privateProtectedField = 0; // no access
    }
}

public class NotInheritedСlass
{
    public void Method()
    {
        ModificatorsClass modificatorsClassInstance = new ModificatorsClass();

        modificatorsClassInstance.publicField = 0; // access is available
        modificatorsClassInstance.internalField = 0; // no access
        modificatorsClassInstance.protectedField = 0; // no access
        modificatorsClassInstance.privateField = 0; // no access
        modificatorsClassInstance.protectedInternalField = 0; // no access
        modificatorsClassInstance.privateProtectedField = 0; // no access
    }
}