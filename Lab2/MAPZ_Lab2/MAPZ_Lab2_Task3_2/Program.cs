﻿public class InheritedСlass : ClassWithoutAccessMofifier // 'ClassWithoutAccessMofifier' is inaccessible due to its protection level
{
    public void Method()
    {
        ClassWithoutAccessMofifier classWithoutAccessMofifier = new ClassWithoutAccessMofifier(); // 'ClassWithoutAccessMofifier' is inaccessible due to
                                                                                                  // its protection level
        StructWithoutAccessModifier structWithoutAccessModifier = new StructWithoutAccessModifier(); // 'StructWithoutAccessModifier' is inaccessible due to
                                                                                                     // its protection level
        InterfaceWithoutAccessModifier interfaceWithoutAccessModifier; // 'InterfaceWithoutAccessModifier' is inaccessible due to its protection level
    }
}

public class NotInheritedСlass
{
    public void Method()
    {
        ClassWithoutAccessMofifier classWithoutAccessMofifier = new ClassWithoutAccessMofifier(); // 'ClassWithoutAccessMofifier' is inaccessible due to
                                                                                                  // its protection level
        StructWithoutAccessModifier structWithoutAccessModifier = new StructWithoutAccessModifier(); // 'StructWithoutAccessModifier' is inaccessible due to
                                                                                                     // its protection level
        InterfaceWithoutAccessModifier interfaceWithoutAccessModifier; // 'InterfaceWithoutAccessModifier' is inaccessible due to its protection level
    }
}