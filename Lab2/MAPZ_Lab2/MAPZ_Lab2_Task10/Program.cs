﻿// Demonstrate boxing/unboxing.

internal class Program
{
    private static void Main(string[] args)
    {
        int var = 10;

        object obj = var; // boxing

        int var2 = (int)obj; // unboxing
    }
}