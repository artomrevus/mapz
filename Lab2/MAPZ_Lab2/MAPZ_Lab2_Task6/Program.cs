﻿// Implement multiple inheritance in C#.

using System.Drawing;


internal interface IElectricalDevice
{
    void On();
    void Off();
}

internal interface IRadioSignalReceiver
{
    void ChangeFrequency(double frequency);
}

internal interface ISoundDevice
{
    void ChangeVolume(uint volume);
}

internal interface IRadio : IElectricalDevice, IRadioSignalReceiver, ISoundDevice
{
    string Model { get; }
    Color Color { get; }
    double CurrentFrequency { get; set; }
    double CurrentVolume { get; set; }
}

internal class Radio : IRadio, IElectricalDevice, IRadioSignalReceiver, ISoundDevice
{
    public string Model { get; }
    public Color Color { get; }
    public double CurrentFrequency { get; set; }
    public double CurrentVolume { get; set; }
    
    public Radio(string model, Color color)
    {
        Model = model;
        Color = color;
        CurrentFrequency = 0.0;
        CurrentVolume = 0.0;
    }

    public void ChangeFrequency(double frequency)
    {
        CurrentFrequency = frequency;
    }

    public void ChangeVolume(uint volume)
    {
        CurrentVolume = volume;
    }

    public void Off()
    {
        Console.WriteLine("Silence...");
    }

    public void On()
    {
        Console.WriteLine("♪♫♬");
    }
}